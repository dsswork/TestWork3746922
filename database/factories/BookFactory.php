<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Book>
 */
class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $title = $this->faker->sentence;

        return [
            'title' => $title,
            'slug' => Str::of($title)->remove(['\'', '.'])->lower()->kebab(),
            'description' => $this->faker->paragraphs(5, true),
            'text' => $this->faker->paragraphs(10, true),
            'published_at' => $this->faker->dateTime
        ];
    }
}
