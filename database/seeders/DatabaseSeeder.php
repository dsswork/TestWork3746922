<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Author::factory(20)->create();
        Book::factory(100)->create();

        for($i = 0; $i<100; $i++) {
            try {
                DB::table('author_book')->insert([
                    'author_id' => rand(1, 20),
                    'book_id' => $i
                ]);
            } catch(QueryException) {
                continue;
            }
        }
    }
}
