<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'api_key'], function () {
    Route::controller(BookController::class)->group(function () {
        Route::get('books', 'index');
        Route::post('books', 'store');
        Route::get('books/{id}', 'show');
        Route::put('books/{id}', 'update');
        Route::delete('books/{id}', 'destroy');
    });

    Route::controller(AuthorController::class)->group(function () {
        Route::get('authors', 'index');
        Route::post('authors', 'store');
        Route::get('authors/{id}', 'show');
        Route::put('authors/{id}', 'update');
        Route::delete('authors/{id}', 'destroy');
    });
});
