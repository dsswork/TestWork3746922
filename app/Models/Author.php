<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'birthday', 'biography'];

    public function books()
    {
        return $this->belongsToMany(Book::class);
    }

    public function scopeSort($query, $field, $direction)
    {
        if($field) {
            if (!$direction) $direction = 'asc';
            $query->orderBy($field, $direction);
        }
    }
}
