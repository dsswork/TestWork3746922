<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'description', 'text', 'published_at'];

    public function authors()
    {
        return $this->belongsToMany(Author::class);
    }

    public function scopeSearch($query, array $filters)
    {
        $query->when($filters['search'] ?? false, fn($query, $search) =>
            $query->where(fn($query) => $query
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('description', 'like', '%' . $search . '%')
                ->orWhere('text', 'like', '%' . $search . '%')
                ->orWhere(fn($query) => $query->whereHas('authors', fn ($query) =>
                    $query->where('name', 'like', '%' . $search . '%')
                ))
            )
        );
    }

    public function scopeSort($query, $field, $direction)
    {
        if($field) {
            if (!$direction) $direction = 'asc';
            $query->orderBy($field, $direction);
        }
    }

}
