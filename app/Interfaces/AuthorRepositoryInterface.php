<?php

namespace App\Interfaces;

use App\Http\Requests\AuthorFilterRequest;
use App\Http\Requests\AuthorIdRequest;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;


interface AuthorRepositoryInterface
{
    public function all(AuthorFilterRequest $request);
    public function create(StoreAuthorRequest $request);
    public function update(UpdateAuthorRequest $request);
    public function delete(AuthorIdRequest $request);
    public function find(AuthorIdRequest $request);
}
