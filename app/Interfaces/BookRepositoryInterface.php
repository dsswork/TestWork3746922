<?php

namespace App\Interfaces;

use App\Http\Requests\BookFilterRequest;
use App\Http\Requests\BookIdRequest;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;

interface BookRepositoryInterface
{
    public function all(BookFilterRequest $request);
    public function create(StoreBookRequest $request);
    public function update(UpdateBookRequest $request);
    public function delete(BookIdRequest $request);
    public function find(BookIdRequest $request);
}
