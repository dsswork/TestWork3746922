<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AuthenticateApi extends Middleware
{
    protected function authenticate($request, array $guards)
    {
        $key = $request->bearerToken();
        if($key === config('apikey')) return;
        $this->unauthenticated($request, $guards);
    }
}
