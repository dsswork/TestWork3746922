<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'int|exists:App\Models\Book,id',
            'title' => 'string|required',
            'slug' => 'string|required',

            'description' => 'string|required',
            'text' => 'string|required',
            'published_at' => 'date',
        ];
    }
}
