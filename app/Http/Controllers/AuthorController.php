<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorFilterRequest;
use App\Http\Requests\AuthorIdRequest;
use App\Interfaces\AuthorRepositoryInterface;
use App\Models\Author;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;

class AuthorController extends Controller
{
    private AuthorRepositoryInterface $authorRepository;

    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AuthorFilterRequest $request)
    {
        return $this->authorRepository->all($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAuthorRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthorRequest $request)
    {
        $this->authorRepository->create($request);
        return response(['created author' => Author::with('books')->latest()->first()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(AuthorIdRequest $request)
    {
        return $this->authorRepository->find($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAuthorRequest  $request
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthorRequest $request)
    {
        return $this->authorRepository->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuthorIdRequest $request)
    {
        return response(['deleted author' => $this->authorRepository->delete($request)]);
    }
}
