<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookFilterRequest;
use App\Http\Requests\BookIdRequest;
use App\Interfaces\BookRepositoryInterface;
use App\Models\Book;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;

class BookController extends Controller
{
    private BookRepositoryInterface $bookRepository;

    public function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BookFilterRequest $request)
    {
        return $this->bookRepository->all($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookRequest $request)
    {
        $this->bookRepository->create($request);
        return response(['created book' => Book::with('authors')->latest()->first()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(BookIdRequest $request)
    {
        return $this->bookRepository->find($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookRequest  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request)
    {
        return $this->bookRepository->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookIdRequest $request)
    {
        return response(['deleted book' => $this->bookRepository->delete($request)]);
    }
}
