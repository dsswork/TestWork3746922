<?php

namespace App\Repositories;

use App\Http\Requests\AuthorFilterRequest;
use App\Http\Requests\AuthorIdRequest;
use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Interfaces\AuthorRepositoryInterface;
use App\Models\Author;
use Illuminate\Support\Str;

class EloquentAuthorRepository implements AuthorRepositoryInterface
{
    protected $model;

    public function __construct(Author $author)
    {
        $this->model = $author;
    }

    public function all(AuthorFilterRequest $request)
    {
        return $this->model
            ->with('books')
            ->sort($request->sortField, $request->direction)
            ->get();
    }

    public function create(StoreAuthorRequest $request)
    {
        $author = $this->model->create([
            'name' => $request->name,
            'slug' => Str::of($request->name)->remove(['\'', '.'])->lower()->kebab(),
            'birthday' => $request->birthday,
            'biography' => $request->biography,
        ]);

        $books = Author::find($request->book_id);
        $author->books()->attach($books);
    }

    public function update(UpdateAuthorRequest $request)
    {
        $author = $this->findById($request->id);
        $oldAuthor = clone $author;

        $author->update([
            'name' => $request->name,
            'slug' => $request->slug,
            'birthday' => $request->birthday,
            'biography' => $request->biography,
        ]);

        return response(['before update' => $oldAuthor, 'after update' => $author]);
    }

    public function delete(AuthorIdRequest $request)
    {
        $author = $this->findById($request->id);
        $author->delete();
        return $author;
    }

    public function find(AuthorIdRequest $request)
    {
        return $this->findById($request->id);
    }

    private function findById($id)
    {
        return $this->model->with('books')->find($id);
    }
}
