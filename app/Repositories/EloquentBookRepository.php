<?php

namespace App\Repositories;

use App\Http\Requests\BookFilterRequest;
use App\Http\Requests\BookIdRequest;
use App\Http\Requests\StoreBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Interfaces\BookRepositoryInterface;
use App\Models\Author;
use App\Models\Book;
use Illuminate\Support\Str;

class EloquentBookRepository implements BookRepositoryInterface
{
    protected $model;

    public function __construct(Book $book)
    {
        $this->model = $book;
    }

    public function all(BookFilterRequest $request)
    {
        return $this->model
            ->with('authors')
            ->search(['search' => $request->search])
            ->sort($request->sortField, $request->direction)
            ->get();
    }

    public function create(StoreBookRequest $request)
    {
        $book = $this->model->create([
            'title' => $request->title,
            'slug' => Str::kebab(Str::lower($request->title)),
            'description' => $request->description,
            'text' => $request->text,
            'published_at' => $request->published_at
        ]);

        $authors = Author::find($request->author_id);
        $book->authors()->attach($authors);
    }

    public function update(UpdateBookRequest $request)
    {
        $book = $this->findById($request->id);
        $oldBook = clone $book;

        $book->update([
            'title' => $request->title,
            'slug' => $request->slug,
            'description' => $request->description,
            'text' => $request->text,
            'published_at' => $request->published_at
        ]);

        return response(['before update' => $oldBook, 'after update' => $book]);
    }

    public function delete(BookIdRequest $request)
    {
        $book = $this->findById($request->id);
        $book->delete();
        return $book;
    }

    public function find(BookIdRequest $request)
    {
        return $this->findById($request->id);
    }

    private function findById($id)
    {
        return $this->model->with('authors')->find($id);
    }
}
